import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EquipementText extends StatelessWidget {
  final String name;
  final String numSerie;

  const EquipementText({Key? key, required this.name, required this.numSerie})
      : super(key: key);

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 0, 15),
      child: Container(
        width: 400,
        height: 140,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(35, 20, 10, 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.name + "\n",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "n° de série : " + this.numSerie + "\n",
                  style: TextStyle(color: Colors.grey),
                ),
                const Text(
                  "Infos et historique >\n",
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                ),
              ],
            )),
      ),
    );
  }
}
