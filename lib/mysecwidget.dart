import 'package:flutter/material.dart';
import 'package:flutter_application_1/model/eqipement.dart';

import 'widgetEquipement.dart';

class MysecWidget extends StatelessWidget {
  final List<Equipement> Listequipement;

  const MysecWidget({Key? key, required this.Listequipement}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.fromLTRB(16, 5, 10, 10),
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(0, 5, 10, 10),
              child: Text(
                'Xefi Lyon',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            const Text(
              "2507 Avenue de l'Europe",
            ),
            const Text(
              "69140 Rillieux-la-Pape \n",
            ),
            const Text(
              "04 72 83 75 75 \n",
            ),
            const Text(
              "xefi@gmail.com \n",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(35, 6, 10, 10),
              child: Text("Advenit post multos Scudilo Scutariorum tribunus\n"
                  "velamento subagrestis ingenii persuasionis opifex\n"
                  "calius. qui eum adulabili sermone seriis.\n\n"),
            ),
            Text(
              "Équipements (" + Listequipement.length.toString() + ")\n",
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
            ),
            Flexible(
              child: ListView(
                padding: const EdgeInsets.all(5),
                children: List.generate(
                  Listequipement.length,
                  (index) => EquipementText(
                      name: Listequipement[index].name,
                      numSerie: Listequipement[index].numSerie),
                ),
              ),
            ),
          ],
        ),
        /*body: InkWell(
          onTap: ()=> get.toNamed
        )*/
      ),
    );
  }
}
